# Formation Spring - Exercices
 
Les exercices de la formation Spring (les bases) ci-après.

## TP 01 - Exercice 1

1. Ouvrir le projet (projet maven) avec votre éditeur de code java.

	- Ce projet contient le code source d'une application dont voici le diagramme de classe :

	![Diagramme des classes][1]

2. Executez et examinez le code source.


	- Si vous tentez d'executer la classe `AppLauncher` (elle contient une méthode `main`), une exception va être levée de type `org.springframework.beans.factory.NoSuchBeanDefinitionException`. En effet, le code du projet n'est pas complet.

	- Examinez le code de la fonction `main`, puis le fichier xml qui y est référencé. Vous devriez comprendre pourquoi cette exception est levée.

3. Complétez le fichier de configuration Spring pour :

	- instancier les beans illustrés sur le diagramme de classes ci-dessus.

		*Procédez par étape : commencez par le bean manquant appellé par la méthode main, puis de proche en proche, les autres beans.*


	- résoudre les dépendances à l'exécution en injectant les bonnes instances de beans (utilisez, en fonction des contraintes imposées par les classes, la bonne approche pour l'injection)

	Le programme est censé afficher dans la console :

		(...)
		[Référence de réservation : AZERTYUI123]	



[1]: https://bitbucket.org/sformations/formations-spring/downloads/classes_00.gif
 