package com.sqli.formation.spring.ex1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sqli.formation.spring.ex1.domain.Reservation;
import com.sqli.formation.spring.ex1.service.IReservationService;

public class AppLauncher {

	public AppLauncher() {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("/application-context.xml");
		IReservationService service = context.getBean("reservationService", IReservationService.class);
		Reservation reservation = service.getReservation();
		
		System.out.println(reservation);
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new AppLauncher();
	}

}
