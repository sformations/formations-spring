package com.sqli.formation.spring.ex1.service.impl;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;
import com.sqli.formation.spring.ex1.service.IReservationService;

public class DefaultReservationServiceImpl implements IReservationService {

	private IReservationDao reservationDao;
	
	public DefaultReservationServiceImpl(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	@Override
	public Reservation getReservation() {
		return this.reservationDao.fetchReservation();
	}
	
}
