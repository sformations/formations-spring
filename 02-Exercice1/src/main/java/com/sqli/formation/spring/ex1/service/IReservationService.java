package com.sqli.formation.spring.ex1.service;

import com.sqli.formation.spring.ex1.domain.Reservation;

public interface IReservationService {

	public Reservation getReservation();
	
}
