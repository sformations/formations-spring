package com.sqli.formation.spring.ex1.dao.impl;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Billet;
import com.sqli.formation.spring.ex1.domain.Reservation;

public class DefaultReservationDaoImpl implements IReservationDao {

	@Override
	public Reservation fetchReservation() {
		Reservation r = new Reservation();
		r.setReference("AZERTYUI123");
		return r;
	}

	@Override
	public Billet fetchBillet() {
		Billet b = new Billet();
		b.setNumero(12345678);
		return b;
	}

}
