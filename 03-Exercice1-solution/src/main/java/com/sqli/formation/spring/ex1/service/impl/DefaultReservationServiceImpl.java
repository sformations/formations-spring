package com.sqli.formation.spring.ex1.service.impl;

import com.sqli.formation.spring.ex1.domain.Reservation;
import com.sqli.formation.spring.ex1.domain.helper.GestionnaireReservation;
import com.sqli.formation.spring.ex1.service.IReservationService;

public class DefaultReservationServiceImpl implements IReservationService {
	
	private GestionnaireReservation gestionnaireReservation;

	public void setGestionnaireReservation(
			GestionnaireReservation gestionnaireReservation) {
		this.gestionnaireReservation = gestionnaireReservation;
	}

	@Override
	public Reservation getReservation() {
		return this.gestionnaireReservation.getReservation();
	}
	
}
