package com.sqli.formation.spring.ex1.service;

import com.sqli.formation.spring.ex1.domain.Billet;

public interface IBilletService {
	
	public Billet getBillet();

}
