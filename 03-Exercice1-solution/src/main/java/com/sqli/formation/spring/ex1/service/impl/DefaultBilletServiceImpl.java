package com.sqli.formation.spring.ex1.service.impl;

import com.sqli.formation.spring.ex1.domain.Billet;
import com.sqli.formation.spring.ex1.domain.helper.GestionnaireBillet;
import com.sqli.formation.spring.ex1.service.IBilletService;

public class DefaultBilletServiceImpl implements IBilletService {

	private GestionnaireBillet gestionnaireBillet;

	public void setGestionnaireBillet(GestionnaireBillet gestionnaireBillet) {
		this.gestionnaireBillet = gestionnaireBillet;
	}
	
	public DefaultBilletServiceImpl() {
		super();
	}

	@Override
	public Billet getBillet() {
		return this.gestionnaireBillet.getBillet();
	}

}
