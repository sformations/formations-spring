package com.sqli.formation.spring.ex1.dao;

import com.sqli.formation.spring.ex1.domain.Billet;
import com.sqli.formation.spring.ex1.domain.Reservation;

public interface IReservationDao {

	public Reservation fetchReservation();
	
	public Billet fetchBillet();
	
}
