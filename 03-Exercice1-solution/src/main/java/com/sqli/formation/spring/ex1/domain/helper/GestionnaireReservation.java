package com.sqli.formation.spring.ex1.domain.helper;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;

public class GestionnaireReservation {

	private IReservationDao reservationDao;

	public void setReservationDao(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	public Reservation getReservation() {
		return this.reservationDao.fetchReservation();
	}
	
}
