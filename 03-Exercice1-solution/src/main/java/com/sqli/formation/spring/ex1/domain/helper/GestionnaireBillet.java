package com.sqli.formation.spring.ex1.domain.helper;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Billet;

public class GestionnaireBillet {

	private IReservationDao reservationDao;

	public void setReservationDao(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	public Billet getBillet() {
		return this.reservationDao.fetchBillet();
	}

}
