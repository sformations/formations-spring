package com.sqli.formation.spring.ex1.domain;

public class Billet {

	private int numero;
	
	public Billet() {
		super();
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Numéro de billet : " + this.numero;
	}

}
