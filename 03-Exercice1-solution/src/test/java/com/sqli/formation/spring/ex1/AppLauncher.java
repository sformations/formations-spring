package com.sqli.formation.spring.ex1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sqli.formation.spring.ex1.domain.Billet;
import com.sqli.formation.spring.ex1.domain.Reservation;
import com.sqli.formation.spring.ex1.service.IBilletService;
import com.sqli.formation.spring.ex1.service.IReservationService;

public class AppLauncher {

	public AppLauncher() {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("/application-context.xml");
		
		IReservationService reservationService = context.getBean("reservationService", IReservationService.class);
		IBilletService billetService = context.getBean("billetService", IBilletService.class);
		
		Reservation reservation = reservationService.getReservation();
		Billet billet = billetService.getBillet();
		
		System.out.println(reservation);
		System.out.println(billet);
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new AppLauncher();
	}

}
