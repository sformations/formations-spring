package com.sqli.formation.spring.ex1.service;

import java.util.List;

import com.sqli.formation.spring.ex1.domain.Reservation;

public interface IReservationService {

	public List<Reservation> getAllReservations();
	
}
