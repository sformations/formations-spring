package com.sqli.formation.spring.ex1.domain.helper;

import java.util.List;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;

public class GestionnaireReservation {

	private IReservationDao reservationDao;

	public void setReservationDao(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	public List<Reservation> getAllReservations() {
		return this.reservationDao.fetchAllReservations();
	}
	
}
