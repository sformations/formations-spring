package com.sqli.formation.spring.ex1.service.impl;

import java.util.List;

import com.sqli.formation.spring.ex1.domain.Reservation;
import com.sqli.formation.spring.ex1.domain.helper.GestionnaireReservation;
import com.sqli.formation.spring.ex1.service.IReservationService;

public class DefaultReservationServiceImpl implements IReservationService {
	
	private GestionnaireReservation gestionnaireReservation;

	public void setGestionnaireReservation(
			GestionnaireReservation gestionnaireReservation) {
		this.gestionnaireReservation = gestionnaireReservation;
	}

	@Override
	public List<Reservation> getAllReservations() {
		return this.gestionnaireReservation.getAllReservations();
	}
	
}
