package com.sqli.formation.spring.ex1.domain;

public class Reservation {

	private String reference;
	
	public Reservation() {
		super();
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Override
	public String toString() {
		return "Référence de réservation : " + this.reference;
	}

}
