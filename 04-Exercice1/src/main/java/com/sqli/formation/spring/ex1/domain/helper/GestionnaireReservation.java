package com.sqli.formation.spring.ex1.domain.helper;

import java.util.List;


import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;

// Annotation à ajouter sur cette classe pour en faire un bean
public class GestionnaireReservation {

	// Annotation à ajouter ici pour injecter l'un des deux bean DAO
	private IReservationDao reservationDao;

	public void setReservationDao(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	public List<Reservation> getAllReservations() {
		return this.reservationDao.fetchAllReservations();
	}
	
}
