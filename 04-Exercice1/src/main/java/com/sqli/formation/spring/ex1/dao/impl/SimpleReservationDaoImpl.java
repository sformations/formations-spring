package com.sqli.formation.spring.ex1.dao.impl;

import java.util.ArrayList;
import java.util.List;


import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;

// Annotation à ajouter sur cette classe pour en faire un bean
public class SimpleReservationDaoImpl implements IReservationDao {

	@Override
	public List<Reservation> fetchAllReservations() {
		
		List<Reservation> reservations = new ArrayList<Reservation>();
		
		Reservation r = new Reservation();
		r.setReference("WXCVBN345");
		reservations.add(r);
		
		return reservations;
		
	}

}
