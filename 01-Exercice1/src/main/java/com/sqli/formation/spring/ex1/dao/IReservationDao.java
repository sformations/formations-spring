package com.sqli.formation.spring.ex1.dao;

import java.util.List;

import com.sqli.formation.spring.ex1.domain.Reservation;

public interface IReservationDao {

	public List<Reservation> fetchAllReservations();
	
}
