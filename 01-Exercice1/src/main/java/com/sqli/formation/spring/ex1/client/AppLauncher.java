package com.sqli.formation.spring.ex1.client;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sqli.formation.spring.ex1.domain.Reservation;
import com.sqli.formation.spring.ex1.service.IReservationService;

public class AppLauncher {

	public AppLauncher() {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("/application-context.xml");
		IReservationService service = context.getBean("reservationService", IReservationService.class);
		List<Reservation> reservations = service.getAllReservations();
		
		System.out.println(reservations);
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new AppLauncher();
	}

}
