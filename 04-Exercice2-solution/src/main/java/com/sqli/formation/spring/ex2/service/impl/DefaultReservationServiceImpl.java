package com.sqli.formation.spring.ex2.service.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.sqli.formation.spring.ex2.domain.Reservation;
import com.sqli.formation.spring.ex2.domain.helper.GestionnaireReservation;
import com.sqli.formation.spring.ex2.service.IReservationService;

@Named("reservationService")
public class DefaultReservationServiceImpl implements IReservationService {
	
	@Inject
	private GestionnaireReservation gestionnaireReservation;

	public void setGestionnaireReservation(
			GestionnaireReservation gestionnaireReservation) {
		this.gestionnaireReservation = gestionnaireReservation;
	}

	@Override
	public List<Reservation> getAllReservations() {
		return this.gestionnaireReservation.getAllReservations();
	}
	
}
