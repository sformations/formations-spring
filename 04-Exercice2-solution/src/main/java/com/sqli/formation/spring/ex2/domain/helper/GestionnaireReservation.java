package com.sqli.formation.spring.ex2.domain.helper;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.sqli.formation.spring.ex2.dao.IReservationDao;
import com.sqli.formation.spring.ex2.domain.Reservation;

@Named("gestionnaireReservation")
public class GestionnaireReservation {

	@Inject
	@Named("simpleReservationDao")
	private IReservationDao reservationDao;

	public void setReservationDao(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	public List<Reservation> getAllReservations() {
		return this.reservationDao.fetchAllReservations();
	}
	
}
