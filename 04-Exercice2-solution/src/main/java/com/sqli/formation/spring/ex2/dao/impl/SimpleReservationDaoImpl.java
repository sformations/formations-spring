package com.sqli.formation.spring.ex2.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import com.sqli.formation.spring.ex2.dao.IReservationDao;
import com.sqli.formation.spring.ex2.domain.Reservation;

@Named("simpleReservationDao")
public class SimpleReservationDaoImpl implements IReservationDao {

	@Override
	public List<Reservation> fetchAllReservations() {
		
		List<Reservation> reservations = new ArrayList<Reservation>();
		
		Reservation r = new Reservation();
		r.setReference("WXCVBN345");
		reservations.add(r);
		
		return reservations;
		
	}

}
