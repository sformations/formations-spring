package com.sqli.formation.spring.ex2.service;

import java.util.List;

import com.sqli.formation.spring.ex2.domain.Reservation;

public interface IReservationService {

	public List<Reservation> getAllReservations();
	
}
