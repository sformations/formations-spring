package com.sqli.formation.spring.ex1.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;

@Repository("simpleReservationDao")
public class SimpleReservationDaoImpl implements IReservationDao {

	@Override
	public List<Reservation> fetchAllReservations() {
		
		List<Reservation> reservations = new ArrayList<Reservation>();
		
		Reservation r = new Reservation();
		r.setReference("WXCVBN345");
		reservations.add(r);
		
		return reservations;
		
	}

}
