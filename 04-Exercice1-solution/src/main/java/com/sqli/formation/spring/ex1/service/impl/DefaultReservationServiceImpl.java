package com.sqli.formation.spring.ex1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sqli.formation.spring.ex1.domain.Reservation;
import com.sqli.formation.spring.ex1.domain.helper.GestionnaireReservation;
import com.sqli.formation.spring.ex1.service.IReservationService;

@Service("reservationService")
public class DefaultReservationServiceImpl implements IReservationService {
	
	@Autowired
	private GestionnaireReservation gestionnaireReservation;

	public void setGestionnaireReservation(
			GestionnaireReservation gestionnaireReservation) {
		this.gestionnaireReservation = gestionnaireReservation;
	}

	@Override
	public List<Reservation> getAllReservations() {
		return this.gestionnaireReservation.getAllReservations();
	}
	
}
