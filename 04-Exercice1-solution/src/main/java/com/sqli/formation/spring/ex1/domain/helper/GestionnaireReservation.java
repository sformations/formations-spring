package com.sqli.formation.spring.ex1.domain.helper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;

@Component("gestionnaireReservation")
public class GestionnaireReservation {

	@Autowired
	@Qualifier("defaultReservationDao")
	private IReservationDao reservationDao;

	public void setReservationDao(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	public List<Reservation> getAllReservations() {
		return this.reservationDao.fetchAllReservations();
	}
	
}
