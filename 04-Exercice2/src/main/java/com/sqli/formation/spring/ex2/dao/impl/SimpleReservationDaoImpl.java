package com.sqli.formation.spring.ex2.dao.impl;

import java.util.ArrayList;
import java.util.List;


import com.sqli.formation.spring.ex2.dao.IReservationDao;
import com.sqli.formation.spring.ex2.domain.Reservation;

// Annotation JSR (javax.inject) a ajouter ici pour en faire un bean
public class SimpleReservationDaoImpl implements IReservationDao {

	@Override
	public List<Reservation> fetchAllReservations() {
		
		List<Reservation> reservations = new ArrayList<Reservation>();
		
		Reservation r = new Reservation();
		r.setReference("WXCVBN345");
		reservations.add(r);
		
		return reservations;
		
	}

}
