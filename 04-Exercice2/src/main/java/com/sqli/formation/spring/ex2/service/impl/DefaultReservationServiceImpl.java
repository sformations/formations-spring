package com.sqli.formation.spring.ex2.service.impl;

import java.util.List;

import javax.inject.Named;

import com.sqli.formation.spring.ex2.domain.Reservation;
import com.sqli.formation.spring.ex2.domain.helper.GestionnaireReservation;
import com.sqli.formation.spring.ex2.service.IReservationService;

// Annotation JSR (javax.inject) a ajouter ici pour en faire un bean
public class DefaultReservationServiceImpl implements IReservationService {
	
	// Annotation JSR (javax.inject) a ajouter ici pour en faire un bean
	private GestionnaireReservation gestionnaireReservation;

	public void setGestionnaireReservation(
			GestionnaireReservation gestionnaireReservation) {
		this.gestionnaireReservation = gestionnaireReservation;
	}

	@Override
	public List<Reservation> getAllReservations() {
		return this.gestionnaireReservation.getAllReservations();
	}
	
}
