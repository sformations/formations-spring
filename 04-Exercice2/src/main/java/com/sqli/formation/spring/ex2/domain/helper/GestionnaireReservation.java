package com.sqli.formation.spring.ex2.domain.helper;

import java.util.List;


import com.sqli.formation.spring.ex2.dao.IReservationDao;
import com.sqli.formation.spring.ex2.domain.Reservation;

// Annotation JSR (javax.inject) a ajouter ici pour en faire un bean
public class GestionnaireReservation {

        // Annotation JSR (javax.inject) a ajouter ici pour en faire un bean
	private IReservationDao reservationDao;

	public void setReservationDao(IReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}
	
	public List<Reservation> getAllReservations() {
		return this.reservationDao.fetchAllReservations();
	}
	
}
