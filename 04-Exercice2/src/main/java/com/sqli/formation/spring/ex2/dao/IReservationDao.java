package com.sqli.formation.spring.ex2.dao;

import java.util.List;

import com.sqli.formation.spring.ex2.domain.Reservation;

public interface IReservationDao {

	public List<Reservation> fetchAllReservations();
	
}
