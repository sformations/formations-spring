package com.sqli.formation.spring.ex1.dao.impl;

import com.sqli.formation.spring.ex1.dao.IReservationDao;
import com.sqli.formation.spring.ex1.domain.Reservation;

public class DefaultReservationDaoImpl implements IReservationDao {

	private String referenceBase;

	public void setReferenceBase(String referenceBase) {
		this.referenceBase = referenceBase;
	}
	
	@Override
	public Reservation fetchReservation() {
		
		Reservation r = new Reservation();
		r.setReference(this.referenceBase);
		
		return r;
		
	}

}
